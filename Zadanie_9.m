
clc;
clear;
clc;

myFileN = 'z3_1350V_x20.dat';  %NAME OF FILE
myFile = dir(myFileN);         
myFileId=fopen(myFileN, 'r');  %OPEN THE FILE
samples = 256                     %NUMBER OF SAMPLES (MORE SAMPLES MORE MEMMORY USAGE)
arraySize = 8400000000 /samples; %DIVIDE FILE BYTES BY SAMPLES
n = zeros(1, samples-1);

for i = 1:fix(myFile.bytes/arraySize)
    data = fread(myFileId, arraySize, 'uint8');
    for j = 1:arraySize
        n(data(j)) = n(data(j)) + 1;
    end
    disp(i*100/(samples+5));disp('%');% DISPLAY PROGRESS IN %   100%-COMPE
end
noise = NoiseLevel(n);
fprintf('Noise level: %d \n', noise); 
fclose(myFileId);   % CLOSE FILE

histogram(n);
ylabel('Level');%PLOT HISTOGRAM
xlabel('Sample');
title('Histogram of noise');
myFileN = fopen('z3_1350V_x20_peak_analysis_results.dat ','w'); % CREATE NEW FILE AND OPEN IT
fprintf(myFileN, 'Noise level: %d\n', noise);  % WRITE NOISE TO FILE
fclose(myFileN);       % CLOSE FILE

function NOISE = NoiseLevel(data)         % NOISE FUNCTION RETURNS MOST OCCURED VALUE IN FILE
    [index] = find(data == max(data));
    NOISE = index;
end
